import argparse

# Parameter Parsing Set Up
parser = argparse.ArgumentParser()
parser.add_argument('-p', '--primes', help='Prime Numbers', type=int, nargs='+')

# Store args
args = parser.parse_args()

def isPrime(num):
    if num > 2:
        for i in range(2, num):
            if not (num % i):
                return False
    else:
        return False
    return True

def determinePrimes(prime):
    n = prime
    m = 0

    for i in range(2, prime):
        if(isPrime(m) and isPrime(n) and (n + m == prime)):
            break
        else:
            n -= 1
            m += 1

    print(str(n) + " + " + str(m) + " = " + str(prime))

    # Example output
    # 2 + 3 = 5
    # 2 + 5 = 7

if __name__ == '__main__':
    for p in args.primes:
        determinePrimes(p)