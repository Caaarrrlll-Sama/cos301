import linecache, array

lines = array.array('i', [85, 124, 1984, 3, 901, 3, 8546, 5, 3, 85, 3437])
words = array.array('i', [8, 11, 8, 5, 1, 13, 12, 2, 4, 10, 7])
list = []
wordList = []

def findCode():
    books = "The Count of Monte Cristo"

    #get the lines in the book and save to array
    for i in range(0, len(lines)):
        list.append(linecache.getline(books, lines[i]))

    #get the specific words from those lines
    for i in range(0, len(list)):
        tempList =  list[i].split()
        wordList.append(tempList[words[i] - 1])

    #build 1 string with the words
    passphase = " ".join(wordList)

    print(books)
    print(passphase)

    # Example output:
    # Tom Sawyer
    # Some deep life-changing quote

if __name__ == '__main__':
    findCode()